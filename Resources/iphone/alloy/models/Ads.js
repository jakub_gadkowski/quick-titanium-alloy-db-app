exports.definition = {
    config: {
        columns: {
            id: "INTEGER PRIMARY KEY",
            name: "text",
            advertiser: "text"
        },
        adapter: {
            type: "sql",
            collection_name: "ads",
            db_name: "adsDB",
            idAttribute: "id"
        }
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("ads", exports.definition, [ function(migration) {
    migration.name = "ads";
    migration.id = "20140304120725";
    var preload_data = [ {
        name: "Super Ad",
        advertiser: "ezclick GmbH"
    }, {
        name: "Shit Ad",
        advertiser: "ACME Ltd."
    }, {
        name: "One More Ad",
        advertiser: "Noname Company"
    } ];
    migration.up = function(migrator) {
        migrator.createTable({
            columns: {
                id: "INTEGER PRIMARY KEY",
                name: "text",
                advertiser: "text"
            }
        });
        for (var i = 0; preload_data.length > i; i++) migrator.insertRow(preload_data[i]);
    };
    migration.down = function(migrator) {
        migrator.dropTable();
    };
} ]);

collection = Alloy.C("ads", exports.definition, model);

exports.Model = model;

exports.Collection = collection;