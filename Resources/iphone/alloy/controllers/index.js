function Controller() {
    function showAds() {
        Ti.API.info("fired");
        var ads = Alloy.createController("ads").getView();
        $.navigationWin.openWindow(ads);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.__alloyId11 = Ti.UI.createWindow({
        backgroundColor: "white",
        title: "Test App",
        id: "__alloyId11"
    });
    $.__views.label = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        backgroundColor: "#888888",
        text: "Show me Ads, please!",
        id: "label"
    });
    $.__views.__alloyId11.add($.__views.label);
    showAds ? $.__views.label.addEventListener("click", showAds) : __defers["$.__views.label!click!showAds"] = true;
    $.__views.navigationWin = Ti.UI.iOS.createNavigationWindow({
        window: $.__views.__alloyId11,
        id: "navigationWin"
    });
    $.__views.navigationWin && $.addTopLevelView($.__views.navigationWin);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.navigationWin.open();
    __defers["$.__views.label!click!showAds"] && $.__views.label.addEventListener("click", showAds);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;