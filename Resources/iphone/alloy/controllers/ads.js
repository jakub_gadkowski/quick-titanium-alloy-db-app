function Controller() {
    function __alloyId10(e) {
        if (e && e.fromAdapter) return;
        __alloyId10.opts || {};
        var models = __alloyId9.models;
        var len = models.length;
        var rows = [];
        for (var i = 0; len > i; i++) {
            var __alloyId2 = models[i];
            __alloyId2.__transform = {};
            var __alloyId4 = Ti.UI.createTableViewRow({
                layout: "composite",
                height: "40dp"
            });
            rows.push(__alloyId4);
            var __alloyId6 = Ti.UI.createLabel({
                left: 0,
                text: "undefined" != typeof __alloyId2.__transform["name"] ? __alloyId2.__transform["name"] : __alloyId2.get("name")
            });
            __alloyId4.add(__alloyId6);
            var __alloyId8 = Ti.UI.createLabel({
                right: 0,
                text: "undefined" != typeof __alloyId2.__transform["advertiser"] ? __alloyId2.__transform["advertiser"] : __alloyId2.get("advertiser")
            });
            __alloyId4.add(__alloyId8);
        }
        $.__views.adsTable.setData(rows);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "ads";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    Alloy.Collections.instance("ads");
    $.__views.ads = Ti.UI.createWindow({
        backgroundColor: "red",
        id: "ads",
        title: "My Ads"
    });
    $.__views.ads && $.addTopLevelView($.__views.ads);
    $.__views.adsTable = Ti.UI.createTableView({
        id: "adsTable"
    });
    $.__views.ads.add($.__views.adsTable);
    var __alloyId9 = Alloy.Collections["ads"] || ads;
    __alloyId9.on("fetch destroy change add remove reset", __alloyId10);
    exports.destroy = function() {
        __alloyId9.off("fetch destroy change add remove reset", __alloyId10);
    };
    _.extend($, $.__views);
    $.ads.addEventListener("close", function() {
        $.destroy();
    });
    var library = Alloy.Collections.ads;
    library.fetch();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;