exports.definition = {
    config: {
        columns: {
            "id": "INTEGER PRIMARY KEY",
            "name": "text",
            "advertiser": "text"
        },
        adapter: {
            "type": "sql",
            "collection_name": "ads",
            "db_name": "adsDB",
            "idAttribute": "id"
        }
    }
    // These two below are only needed if you actually extend the Backbone model, if not you do not need them.
    
    // extendModel: function(Model) {
    //     _.extend(Model.prototype, {
    //         // extended functions and properties go here
    //     });

    //     return Model;
    // },
    // extendCollection: function(Collection) {
    //     _.extend(Collection.prototype, {
    //         // extended functions and properties go here
    //     });

    //     return Collection;
    // }
};
