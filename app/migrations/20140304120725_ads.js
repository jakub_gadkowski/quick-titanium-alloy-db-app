//The silly name of the file is used to determine if there are any new migrations that should overwrite this one.
// check more at http://docs.appcelerator.com/titanium/3.0/#!/guide/Alloy_Sync_Adapters_and_Migrations-section-36739597_AlloySyncAdaptersandMigrations-Migrations

var preload_data = [
            {
                name: "Super Ad",
                advertiser: "ezclick GmbH"
            },
            {
                name: "Shit Ad",
                advertiser: "ACME Ltd."
            },
            {
                name: "One More Ad",
                advertiser: "Noname Company"
            }
];

//Migration up is fired automatically if there is no DB or a new migration files has been added. You can also fire it up manually.
migration.up = function(migrator)
{
    migrator.createTable({
        "columns": {
            "id": "INTEGER PRIMARY KEY",
            "name": "text",
            "advertiser": "text"
        }
    });
    for (var i = 0; i < preload_data.length; i++) {
        migrator.insertRow(preload_data[i]);
    }
};


//migration.down is used to remove the changes that were added by this migration file. 
migration.down = function(migrator)
{
    migrator.dropTable();
};