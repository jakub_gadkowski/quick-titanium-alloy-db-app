//There is no need to pass any parameters to the function, so I have deleted the (e)
// Also there is no need to create the controller before the user clicks on the label so I moved it inside the function
function showAds() {
	Ti.API.info('fired');
    //Lets create a new Controller with the 'ads' View.
    //I use single quotes everywhere because I chose to follow this style guide https://github.com/airbnb/javascript
    var ads = Alloy.createController('ads').getView();
    //Now in 'ads' variable we have fully functional Alloy View

    //Lets slap this 'ads' view into our NavigationWindow stack. We have to use openWindow instead of open() 
    // $ is an Alloy object, navigationWin is an ID of our NavigationWindow
    $.navigationWin.openWindow(ads);
}

//Because our primary object in this view has an ID we cannot use the default:
// $.index.open();
// instead we have to use
$.navigationWin.open();