// Free model-view data binding resources when this view-controller closes
// This is needed to avoid memory leaks
$.ads.addEventListener('close', function() {
    $.destroy();
});

// Fetch the Collection and trigger the synchronization in View
var library = Alloy.Collections.ads;
library.fetch();


